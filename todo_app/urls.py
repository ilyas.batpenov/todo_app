from django.urls import path
from todo_app.views import TaskListView, TaskView, TaskDoneView

urlpatterns = [
    path('', TaskListView.as_view(), name='tasks_list'),
    path('<int:id>/', TaskView.as_view(), name='tasks_list'),
    path('<int:id>/execute/', TaskDoneView.as_view(), name='tasks_list'),

]
