from django.db import models
from django.utils.translation import gettext_lazy as _


class Task(models.Model):
    title = models.CharField(max_length=100, verbose_name=_('Title'))
    description = models.TextField(max_length=5000, verbose_name=_('Description'))
    deadline = models.DateField(verbose_name=_('Deadline'))
    done = models.BooleanField(default=False)
