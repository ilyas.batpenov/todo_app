from django.urls import path, include
from rest_auth.views import LoginView, LogoutView


urlpatterns = [
    path('login/', LoginView.as_view(), name='rest_login'),
    path('logout/', LogoutView.as_view(), name='rest_logout'),
    path('registration/', include('rest_auth.registration.urls')),
]
