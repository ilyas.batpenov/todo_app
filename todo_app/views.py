from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from todo_app.models import Task
from todo_app.serializers import TaskSerializer, TasksListSerializer


class TaskListView(APIView):

    def get(self, request, *args, **kwargs):
        tasks = Task.objects.all()
        serializer = TasksListSerializer(tasks, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = TaskSerializer(data=request.data)
        if serializer.is_valid():
            task = serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=400)


class TaskView(APIView):
    def get(self, request, *args, **kwargs):
        task = get_object_or_404(Task, pk=kwargs['id'])
        serializer = TaskSerializer(task)
        return Response(serializer.data)

    def patch(self, request, *args, **kwargs):
        task = get_object_or_404(Task, pk=kwargs['id'])
        serializer = TaskSerializer(data=request.data, instance=task, partial=True)

        if serializer.is_valid():
            task = serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=400)

    def delete(self, request, *args, **kwargs):
        task = get_object_or_404(Task, pk=kwargs['id'])
        task_title = task.title
        task.delete()
        return Response({'task deleted': task_title})


class TaskDoneView(APIView):
    def post(self, request, *args, **kwargs):
        task = get_object_or_404(Task, pk=kwargs['id'])
        if task.done is False:
            task.done = True
        else:
            task.done = False
        task.save()
        serializer = TaskSerializer(instance=task)
        return Response(serializer.data)
