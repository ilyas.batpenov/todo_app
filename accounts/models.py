from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    email = models.EmailField(unique=True, verbose_name=_('email'))
    username = models.CharField(blank=True, null=True, verbose_name=_("username"), max_length=150, unique=True)
